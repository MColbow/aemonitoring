#Monitoring
import sys
import os
import socket
import platform
import psutil
import time
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart. import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.utils import COMMASPACE
from argparse import Argumentparser
#Argumente für das Ausführen via Konsole
parser = Argumentparser()
    Resource = ""
    parser.add_argument("-d","--disk", Resource = "Disk",help="Set the recoure that has to be mointored to the harddrive/SSD", Metavar="DISK")
    parser.add_argument("-p","--processes", Resource = "Process",help="Set the recource taht needs to be mointored to the number of processes",                                  Metavar="PROCESSES")
    parser.add_argument("-c","--cpu", Resource = "CPU",help="Set the recource that need to be mointored to the CPU occupancy",Metavar="CPU")
    parser.add_argument("-v","--v",Resource = "VRAM",help="Set the recource that needs to be monitored to the virtuel memory",Metavar="VRAM")

args= parser.parse_args()


#E-Mail senden

def mail_send(sender,receiver,subject,message,smtp_Server,username,password,attachment,port=587):
    """
    sender: String mit der Adresse des Senders
    receiver: String mit der Adresse des Empängers
    subject: String mit dem Betreff der Mail
    message: String mit der Nachricht der Mail
    smtp_Server: String des SMTP Servers
    username: Benutzername des Mailfontos des Senders
    password: Password des Mailkontos des Senders
    attachment: möglicher Anhang von dateinen in Form einer Liste von Dateipfaden
    port: Mailport, Standardmäßig ist 587 eingetragen
    return: keins

    """

    #Erstellen eines Mime Objektes(mailobjekt)
    msg=MIMEMultipart()
    msg ["From"] = sender
    msg["to"] = COMMASPACE.join(reciever)
    msg["Subject"] = subject
    msg.attach(MIMEText(message))
    
    #hinzufügen des Anhangs(falls vorhanden)
    #TODO: implement attachment management
   

     #E-Mail senden

   
        #smtp Objekt wird erstellt
        mailserver =smtplib.SMTP(smtp_Server,port)
       
        #am Mailserver indentifizieren
        mailserver.ehlo()

        #Verschlüsseln des Mailversands
        mailserver.startttls()

        #nach Verschlüsselung erneut am Server indentifizieren
        mailserver.ehlo()

        #Anmeldung am Mailserver
        mailserver.login(username,password)
        #senden der Mail
        mailserver.sendmail(sender,receiver,msg.as_string())
        #trennen der Verbindung zum Mailserver
        mailserver.close()



#logdatei
def write_in_logfile(Resource,value=0):
    timestamp = time.strftime("%d.%m.%y %H:%M:%S") #timestamp in einen Strig umwandeln, damit er in file.write benutzt werden kann
    stringvalue = str(value)
    Rechnername = socket.gethostname()
    
    file = open("log.txt","a")
    if Resource == "Disk":
        
        file.write(timestamp + " Diskspace on " + Rechnername + " is currently at " + stringvalue + "GB,and is under the 10GB Limit\n")
    else:
        if Resource == "Process":
        file.write(timestamp + " Numer of Processes on " + Rechnername + " is currently at " + stringvalue + ", and is over the Limit of  400\n")
        else:
            if Resource == "CPU":
                file.write(timestamp + " CPU usage on " + Rechnername + " is currently at " + stringvalue + ",")
            else:
                file.write(timestamp + "VRAM Usage on" + Rechnername + " is currently at " + stringvalue + ",")    

    file.close()
    


#Festplattenspeicher
def get_disk_usage(path="/"):     #hier bitte vor dem AUsführen auf das Betreibssystem anpassen "/" für Unix/Linux/Mac und "C:\" für Windows

   
    disk_usage = psutil.disk_usage(path)
    
    totalSpace = round((disk_usage.total / 2**30), 2)
    usedSpace = round((disk_usage.used / 2**30), 2)
    freeSpace = round((disk_usage.free / 2**30), 2)
    
    percent = disk_usage.percent

    if freeSpace < 10 :
       write_in_logfile("Disk",freeSpace) 
    if freeSpace < 5 :
        mail_send(("fantasiemail@anbieter.de","fantasymail@provider.com","Diskspace too low","The Space on the HDD on" + Rechnername ü "is under the Limit of 5GB",smtp_Server,                         "fantasie","xxxxxxxxxx")         #frei erfundene Maildaten, da sonst echte Maildaten inkl. passwort frei im GIT stehen würden
    return {"total": totalSpace, "used": usedSpace, "free": freeSpace, "percent": percent}



#Anzahl der Prozese
def process_limit():
    processes = len(psutil.pids()) #hier wird die Anzahl der Prozesse ermittelt

    if processes > 400:
        
        print("Number of Processes too high, please terminate processes to reduce them")
        write_in_logfile("Process",processes)
    else:
        if processes > 600:
            mail_send(("fantasiemail@anbieter.de","fantasymail@provider.com","Diskspace too low","The Space on the HDD on" + Rechnername ü "is under the Limit of 5GB",smtp_Server,                         "fantasie","xxxxxxxxxx")         #frei erfundene Maildaten, da sonst echte Maildaten inkl. passwort frei im GIT stehen würden
            else:
                print(processes)

    return { "Prozesse" : processes }   




#CPU Auslastung
def get_CPU_Usage():
    cpu = psutil.cpu_percent
    if cpu > 90:
        wirte_in_logfile("CPU",cpu)
    else:

    return { "CPU Auslastung" : cpu}

#VRAM Benutzung
def get_VRAM():
    vram = psutil.virtual_memory()
    if vram< 90:
        write_in_logfile("VRAM",vram)

    return {"VRAM" : vram}


#main
if Resource ="Disk": 
    print("Festplattenspeicher:",get_disk_usage())
if Resource ="Processes":
    process_limit()
if Rescource ="CPU":
    get_CPU_Usage()
if Resource = "VRAM":
    get_VRAM()


print("Datum",time.strftime("%d.%m.%y %H:%M:%S"))


print("Computer:",platform.system())
        
